import { combineReducers } from 'redux';
import reducer from './bank.reducer';
import bank from './bank.reducer';

const reducers = combineReducers({
  bank: bank,
});

export default reducers;

export type RootState = ReturnType<typeof reducers>;
