import { ActionType } from '../action-types/bank.action-types';
import { Action } from '../actions/bank.actions';

// état initial de la banque
const initialState = 0;

function reducer(state: number = initialState, action: Action) {
  switch (action.type) {
    // Nouvelle todo avec redux saga
    case ActionType.DEPOSIT:
      return state + action.payload;
    case ActionType.WITHDRAW:
      return state - action.payload;
    case ActionType.BANKRUPT:
      return 0;
    default:
      return state;
  }
}

export default reducer;
