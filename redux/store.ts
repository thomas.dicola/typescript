import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { createWrapper } from 'next-redux-wrapper';
import reducers from './reducers';

// middleware
const middleware = [thunk];

// creating store

export const store = createStore(reducers, {}, applyMiddleware(thunk));
// assigning store to next wrapper
const makeStore = () => store;

export const wrapper = createWrapper(makeStore);
