#App next.js

- npm i
- npm run dev
- localhost 3000

#Exercice sur TypeScript

Voici une liste d'animaux et d'actions possibles sur ceux-ci :

Animaux :

- Chat européen (noir)
- Chat chartreux
- Chien Terre-Neuve (noir)
- Chien Moon Moon
- Mésange
- Merle (noir)
- Thon
- Requin
- Asticot

Et les actions possibles:

- On peut photographier tous les animaux
- Tous les chats peuvent miauler
- Tous les chiens peuvent aboyer
- Tous les oiseaux peuvent voler
- Tous les poissons peuvent nager
- On peut caresser tous les animaux à 4 pattes
- On peut nourrir tous les animaux de couleur noire
