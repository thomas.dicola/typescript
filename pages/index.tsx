import type { NextPage } from 'next';
import Head from 'next/head';
import { useDispatch, useSelector } from 'react-redux';
import { bindActionCreators } from 'redux';
import { actionCreators, RootState } from '../redux';

// ---- Typing Promise and Async Request

interface LukeSkywalker {
  name: string;
  height: string;
  mass: string;
  hair_color: string;
  skin_color: string;
  eye_color: string;
  birth_year: string;
  gender: string;
}

export const fetchLukeSkywalker = async (): Promise<LukeSkywalker> => {
  const data = await fetch('https://swapi.dev/api/people/1').then((res) => {
    return res.json();
  });

  return data as LukeSkywalker;
};

const Home: NextPage = () => {
  // ---------------- Découverte de TypeScript ----------------  //

  const n: number = 1;
  const a: string = 'hello';
  const b: boolean = true;
  const d: null = null;
  const arr: any[] = ['aze', 'azr', 3];
  const user: User = {
    firstName: 'John',
    lastName: 'Doe',
  };
  const date: Date = new Date();
  const cb: Function = (e: MouseEvent): void => {};
  type User = { firstName: string; lastName: string };

  // // console.log(user);
  // // console.log(date);
  // // console.log(arr);
  // // console.log(n);
  // // console.log(a);
  // // console.log(b);
  // // console.log(cb);
  // // console.log(d);

  // ---------------- TypeScript : programmation orientée objet, Quête Wild Code School ----------------  //

  interface AnimalGlobal {
    readonly name: string;
    readonly color?: string;
    readonly type: string;
    readonly foot: number;
  }

  class Animal implements AnimalGlobal {
    name: string;
    color?: string;
    type: string;
    foot: number;
    constructor(theName: string, theColor?: string) {
      this.name = theName;
      this.color = theColor;
    }
  }

  class Cat extends Animal {
    type: string;
    foot: number;
    constructor(name: string, color?: string) {
      super(name, color), (this.type = 'Chat');
      this.foot = 4;
    }
  }
  class Dog extends Animal {
    type: string;
    foot: number;
    constructor(name: string, color?: string) {
      super(name, color), (this.type = 'Chien');
      this.foot = 4;
    }
  }
  class Bird extends Animal {
    type: string;
    foot: number;
    constructor(name: string, color?: string) {
      super(name, color), (this.type = 'Oiseau');
      this.foot = 2;
    }
  }
  class Fish extends Animal {
    type: string;
    foot: number;
    constructor(name: string, color?: string) {
      super(name, color), (this.type = 'Poisson');
      this.foot = 0;
    }
  }

  class Maggot extends Animal {
    type: string;
    foot: number;
    constructor(name: string, color?: string) {
      super(name, color), (this.type = 'Asticot');
      this.foot = 0;
    }
  }

  function TakePhoto(animal: Animal) {
    if (animal.type == 'Oiseau' || animal.type == 'Poisson') {
      console.log(`Photo d'un ` + animal.name);
    } else {
      console.log(`Photo d'un ` + animal.type + ' ' + animal.name);
    }
  }

  function Meow(animal: Animal) {
    if (animal.type == 'Chat') {
      console.log('Le ' + animal.type + ' ' + animal.name + ' miaule !');
    } else {
      console.log('Seul les chats peuvent miauler.');
    }
  }

  function Woof(animal: Animal) {
    if (animal.type == 'Chien') {
      console.log('Le ' + animal.type + ' fait woof woof!');
    } else {
      console.log('Cet animal ne peut pas aboyer.');
    }
  }

  function Fly(animal: Animal) {
    if (animal.type == 'Oiseau') {
      console.log('Regarde cette ' + animal.name + ' voler');
    } else {
      console.log('Un ' + animal.type + ' ne sait pas voler.');
    }
  }

  function Swim(animal: Animal) {
    if (animal.type == 'Poisson') {
      console.log('Regarde ce ' + animal.name + ' nager');
    } else {
      console.log('Un ' + animal.type + ' ne sait pas nager.');
    }
  }

  function GoodJob(animal: Animal) {
    if (animal.foot === 4 && animal.color != undefined) {
      console.log(
        'Une caresse pour un ' +
          animal.type +
          ' ' +
          animal.name +
          ' ' +
          animal.color +
          '.'
      );
    } else if (animal.foot == 4 && animal.color == undefined) {
      console.log(
        'Une caresse pour un ' + animal.type + ' ' + animal.name + '.'
      );
    } else {
      console.log('Impossible de carresser un ' + animal.type + ' !');
    }
  }

  function Feed(animal: Animal) {
    if (
      animal.color == 'noir' &&
      animal.type != 'Oiseau' &&
      animal.type != 'Poisson' &&
      animal.type
    ) {
      console.log(
        'Donne à manger à ' +
          animal.type +
          ' ' +
          animal.name +
          ' ' +
          animal.color +
          '.'
      );
    } else if (
      animal.color == 'noir' &&
      (animal.type == 'Oiseau' ||
        animal.type == 'Poisson' ||
        animal.type == 'Asticot')
    ) {
      console.log('Donne à manger à ' + animal.name + ' ' + animal.color + '.');
    } else {
      console.log(
        'On ne peut pas nourir un ' + animal.type + ' de cet couleur'
      );
    }
  }

  let cat1: Cat = new Cat('européen', 'noir');
  let cat2: Cat = new Cat('chartreux');

  let dog1: Dog = new Dog('Terre-Neuve', 'noir');
  let dog2: Dog = new Dog('Moon Moon');

  let bird1: Bird = new Bird('Mésange');
  let bird2: Bird = new Bird('Merle', 'noir');

  let fish1: Fish = new Fish('Thon');
  let fish2: Fish = new Fish('Requin');

  let insecte: Maggot = new Maggot('');

  TakePhoto(cat1);
  TakePhoto(cat2);
  TakePhoto(dog1);
  TakePhoto(dog2);
  TakePhoto(bird1);
  TakePhoto(bird2);
  TakePhoto(fish1);
  TakePhoto(fish2);
  TakePhoto(insecte);

  Meow(cat1);
  Meow(cat2);
  Meow(dog1);
  Meow(dog2);
  Meow(bird1);
  Meow(bird2);
  Meow(fish1);
  Meow(fish2);
  Meow(insecte);

  Woof(cat1);
  Woof(cat2);
  Woof(dog1);
  Woof(dog2);
  Woof(bird1);
  Woof(bird2);
  Woof(fish1);
  Woof(fish2);
  Woof(insecte);

  Fly(cat1);
  Fly(cat2);
  Fly(dog1);
  Fly(dog2);
  Fly(bird1);
  Fly(bird2);
  Fly(fish1);
  Fly(fish2);
  Fly(insecte);

  Swim(cat1);
  Swim(cat2);
  Swim(dog1);
  Swim(dog2);
  Swim(bird1);
  Swim(bird2);
  Swim(fish1);
  Swim(fish2);
  Swim(insecte);

  Feed(cat1);
  Feed(cat2);
  Feed(dog1);
  Feed(dog2);
  Feed(bird1);
  Feed(bird2);
  Feed(fish1);
  Feed(fish2);
  Feed(insecte);

  GoodJob(cat1);
  GoodJob(cat2);
  GoodJob(dog1);
  GoodJob(dog2);
  GoodJob(bird1);
  GoodJob(bird2);
  GoodJob(fish1);
  GoodJob(fish2);
  GoodJob(insecte);

  const dispatch = useDispatch();

  const { depositMoney, withdrawMoney, bankrupt } = bindActionCreators(
    actionCreators,
    dispatch
  );

  const state = useSelector((state: RootState) => state.bank);

  return (
    <div>
      <Head>
        <title>TypeScript</title>
        <meta name='description' content='Generated by create next app' />
        <link rel='icon' href='/favicon.ico' />
      </Head>
      <h1>{state}$</h1>
      <button onClick={() => depositMoney(10)}>Deposit 10$</button>
      <button
        onClick={() => {
          if (state <= 0) return state;
          else withdrawMoney(-10);
        }}
      >
        Withdraw 10$
      </button>
      <button onClick={() => bankrupt()}>Bankrupt</button>
    </div>
  );
};

export default Home;
